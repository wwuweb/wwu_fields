# wwu_fields

A utility feature that provides field bases to other WWU Drupal features.

## Provided Fields

### wwu_date

A date field based on the Date API.

### wwu_file

A file field base on the File module.

### wwu_image

A image field based on the File module.

### wwu_email

An email field based on the user's LDAP email